"""
Created by Joshua Lieder
Production version - v23.04.02
Test version - v23.04.03
"""
import database.queries.gametime as gametime  # Contains functions or classes for querying a database related to tracking game playtime for users.
import secret  # A module containing sensitive information such as API tokens, database credentials, and other secrets that should not be publicly visible.

from difflib import SequenceMatcher  # This import provides a class for comparing sequences and finding the differences between them.
import matplotlib.pyplot as plt  # This import provides a module for creating data visualizations, particularly plots and graphs.
import logs.log as log  # Contains functions or classes related to logging events and messages.
import helptext  # Contains text strings used for help messages or documentation.

# discord _____
import discord  # A Python library for building Discord bots. It provides an easy-to-use interface for interacting with the Discord API. https://guide.pycord.dev
from discord.ui import Button, View

# YouTube _____
from music import suche
from pytube import extract

# Vorbereitung Bot ___________________________________

"""
Preparing the environment for the Discord bot, depending on whether the debug mode is enabled or not.
"""
if secret.get_debug():
    """
    If the debug mode is enabled, the code sets the server ID, bot ID, gametime role, channel
    IDs, log file, and picture folder for the debug environment. Then it creates a
    discord.Bot instance with debug guilds and all intents, adds some logs to the debug
    log file, and copies the database from the live environment to the debug environment.
    Finally, it resets the picture database and game start times.
    """
    server_id = 1056180541961015316  # Testserver
    bot_id = 798473055231541248
    gametime_role = 1056180541961015323

    channel_music = 1056180543944917052
    channel_themen = 1056180544163024940
    channel_einreichen = 1056180544163024941
    channel_archiv = 1056180544163024942
    channel_austausch = 1056180544163024943

    log_file = "main_debug"

    try:
        bot = discord.Bot(debug_guilds=[server_id], intents=discord.Intents.all())
        log.add(log_file, "Server neustart _______________________________________________")
        log.add(log_file, f"Als Server-ID wurde {server_id} festgelegt.")
        log.add(log_file, "Debug Modus aktiviert, kopiere Datenbank aus der Live-Umgebung.")
        gametime.copy_db()

        gametime.clear_start_time()
    except Exception as e:
        text = f"Der Bot hat beim starten ein '{e}' bekommen."
        log.add(log_file, text)
else:
    """
    If the debug mode is not enabled, the code sets the server ID, bot ID, gametime role,
    channel IDs, log file, and picture folder for the live environment. Then it creates a
    discord.Bot instance with the live guild and all intents, resets the picture database
    and game start times, and adds a log message saying that the database is connected.
    """
    server_id = 831447980359286785  # Insel der besonderen Kinder
    bot_id = 745334675706478873
    gametime_role = 939959207824871475

    channel_music = 942494220894236693
    channel_themen = 831447981215580252
    channel_einreichen = 888390763614978098
    channel_archiv = 889487570566807572
    channel_austausch = 888390836511973407

    log_file = "main"

    try:
        bot = discord.Bot(debug_guilds=[server_id], intents=discord.Intents.all())
        log.add(log_file, "Server neustart _______________________________________________")
        log.add(log_file, f"Als Server-ID wurde {server_id} festgelegt.")
        gametime.clear_start_time()
        log.add(log_file, "Datenbank verbunden.")
    except Exception as e:
        text = f"Der Bot hat beim starten ein '{e}' bekommen."
        log.add(log_file, text)


@bot.event
async def on_ready():
    """
    Defines an event listener for the on_ready() event of the bot object.
    This event is triggered when the bot has successfully connected to the Discord API and is ready to start processing events
    """
    log.add(log_file, f"Der Server wurde als Nutzer {bot.user} gestartet.")
    log.add(log_file, "Vorbereitungen abgeschlossen __________________________________")


# Abfrage Befehle __________________________________________________________________________________________

# Musik ___________________________________________________

@bot.event
async def on_message(msg):
    """
    Event listener that is triggered when the bot receives a message. If the message was sent in the `channel_music` channel
    and was not sent by the bot itself, the message is deleted.

    Args:
        msg (discord.Message): The message that was received.
    """
    if msg.channel.id == channel_music:  # Musikchannel
        if msg.author.id != bot_id:  # Bot's
            await msg.delete()


@bot.command(
    name="music",
    description="Musik in den Chat senden")
async def music(interaction: discord.Interaction, link: discord.Option(str, required=True, description="Link zum Lied")):
    """
    Send music to the chat.

    Args:
        :param link: The link to the song.
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        if interaction.channel.id == channel_music:
            youtube_id = extract.video_id(link)
            clean_result = suche(youtube_id)
            genre_str = ", ".join(clean_result['genres'])
            embed = discord.Embed(
                title=f"{clean_result['title']}"
            )
            embed.add_field(name="Genre", value=genre_str.title(), inline=True)
            embed.add_field(name="Release Year", value=clean_result['release'], inline=True)
            embed.set_thumbnail(url=clean_result['cover']['url'])
            embed.set_author(name=clean_result['artist'], icon_url=clean_result['artist_icon']['url'])

            spotify_button = Button(label="Spotify", url=clean_result["spotify_link"])
            youtube_button = Button(label="YouTube", url=clean_result["youtube_link"])
            view = View()
            view.add_item(spotify_button)
            view.add_item(youtube_button)

            text = f"{interaction.user} ({interaction.user.id}) hat /music mit dem Link {link} ausgeführt."
            log.add(log_file, text)
            await interaction.response.send_message(embed=embed, view=view)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /music ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


# Gametime ________________________________________________

@bot.command(
    name="gametime_remove",
    description="Einen speziellen Spiel-Eintrag löschen")
async def gametime_remove(interaction: discord.Interaction, name: discord.Option(str, required=True, description="Name des Spieles")):
    """
    A Discord bot command that allows a user to remove a specific game entry from their game list.
    Args:
        :param name: A required option that specifies the name of the game entry to remove.
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        tmp = gametime.clear_gametime_entry(int(interaction.user.id), name)
        if tmp == 1:
            text = f"**{name}** wurde aus deiner Liste entfernt."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
        elif tmp == 2:
            text = f"Es sind keine Daten bei dir vorhanden."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /gametime_remove ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


@bot.command(
    name="gametime_clear",
    description="Alle deine Spiele-Einträge löschen", )
async def gametime_clear(interaction: discord.Interaction, confirm: discord.Option(str, required=True, description="'Y' zum bestätigen")):
    """
    Deletes all game entries for the user who issued the command.
    Args:
        :param confirm: The confirmation message that the user must enter ('Y') to confirm the deletion of their game entries.
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        if confirm == "Y":
            tmp = gametime.clear_gametime_all(int(interaction.user.id))
            if tmp == 1:
                text = f"Es wurden alle deine Einträge gelöscht."
                log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
                await interaction.response.send_message(text, ephemeral=True)
            elif tmp == 2:
                text = f"Es sind keine Daten bei dir vorhanden."
                log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
                await interaction.response.send_message(text, ephemeral=True)
        else:
            text = f"Es wurden nichts verändert. Bitte bestätige die Löschung im Befehl mit '**Y**'."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /gametime_clear ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


@bot.command(
    name="blacklist_add",
    description="Spiel zur Blacklist hinzufügen", )
async def blacklist_add(interaction: discord.Interaction, name: discord.Option(str, required=True, description="Name des Spieles")):
    """
    Adds a game to the user's blacklist.
    Args:
        :param name: The name of the game to add to the blacklist.
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        tmp = gametime.set_blacklist(int(interaction.user.id), name)
        if tmp == 1:
            text = f"**{name}** wurde zu deiner Blacklist hinzugefügt."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /blacklist_add ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


@bot.command(
    name="blacklist_remove",
    description="Spiel von der Blacklist entfernen", )
async def blacklist_remove(interaction: discord.Interaction, name: discord.Option(str, required=True, description="Name des Spieles")):
    """
    Command to remove a game from the user's blacklist.
    Args:
        :param name: The name of the game to be removed from the user's blacklist.
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        tmp = gametime.clear_blacklist_entry(int(interaction.user.id), name)
        if tmp == 1:
            text = f"**{name}** wurde von deiner Blacklist entfernt."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
        elif tmp == 2:
            text = f"**{name}** wurde nicht in deiner Blacklist gefunden."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /blacklist_remove ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


@bot.command(
    name="blacklist_clear",
    description="Alle deine Blacklist-Einträge löschen", )
async def blacklist_clear(interaction: discord.Interaction, confirm: discord.Option(str, required=True, description="'Y' zum bestätigen")):
    """
    Deletes all the entries in the user's blacklist if the confirmation parameter 'confirm' is 'Y'.
    Args:
        :param confirm: The confirmation parameter for the deletion of all blacklist entries.
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        if confirm == "Y":
            tmp = gametime.clear_blacklist_all(int(interaction.user.id))
            if tmp == 1:
                text = f"Es wurden alle deine Einträge gelöscht."
                log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
                await interaction.response.send_message(text, ephemeral=True)
            elif tmp == 2:
                text = f"Deine Blacklist ist leer, es gibt nichts zu löschen!"
                log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
                await interaction.response.send_message(text, ephemeral=True)
        else:
            text = f"Es wurden nichts verändert. Bitte bestätige die Löschung im Befehl mit '**Y**'."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /blacklist_clear ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


@bot.command(
    name="blacklist_get",
    description="Zeigt alle Blacklist Einträge", )
async def blacklist_get(interaction: discord.Interaction):
    """
    Returns a list of all the games added by the user to their blacklist.
    Args:
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        tmp = gametime.get_blacklist(int(interaction.user.id))
        if len(tmp) == 0:
            text = "Deine Blacklist ist leer."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
        else:
            if len(tmp) == 1:
                text = "Du hast folgendes Spiel auf deiner Blacklist:\n"
            else:
                text = "Du hast folgende Spiele auf deiner Blacklist:\n"
            for i in tmp:
                text = text + "`" + i[0] + "`" + "\n"
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /blacklist_clear ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


@bot.command(
    name="gametime_all",
    description="Gesamtspielzeit als Liste und Kuchen senden lassen", )
async def gametime_all(interaction: discord.Interaction):
    """
    A Discord bot command that sends a pie chart and list of total game time for the user.
    Args:
        :param interaction: Represents a Discord interaction between User and Server
    """
    tmp = gametime.get_games(int(interaction.user.id))

    try:
        if len(tmp) != 0:
            labels = []
            sizes = []
            other_time = 0
            for item in tmp:
                if len(labels) >= 8:
                    other_time += float(item[1])
                else:
                    labels.append(item[0])
                    sizes.append(item[1])

            labels.append("Andere")
            sizes.append(other_time)
            sizes_int = list(map(float, sizes))
            sizes_int = [round(x / 3600, 2) for x in sizes_int]
            merge = list(zip(labels, sizes_int))
            fig1, ax1 = plt.subplots(figsize=(10, 8))
            ax1.pie(sizes, labels=labels, startangle=90, labeldistance=None)
            ax1.legend(["%s - %s Stunden" % x for x in merge], bbox_to_anchor=(1.08, 0.2), loc="upper right")

            ax1.set_title(f"Spielzeit von {interaction.user}", y=1.03)
            ax1.axis('equal')
            plt.savefig(f'plots/{int(interaction.user.id)}.png')

            text = "```"
            for i in tmp:
                name = i[0]
                game_time = float(i[1])
                if game_time >= 3600:
                    game_time = round(game_time / 3600, 2)
                    text = text + f"{name} - {game_time} Stunden\n"
                else:
                    game_time = round(game_time / 60, 2)
                    text = text + f"{name} - {game_time} Minuten\n"
                if len(text) > 1800:
                    text = text + "```"
                    await interaction.user.send(text)
                    text = "```"
            text = text + "```"
            log.add(log_file, f"{interaction.user} / {interaction.user.id} - Gesamtspielzeit als Liste und Kuchen wurde als PM versendet")
            msg = await interaction.user.send(text, file=discord.File(f'plots/{int(interaction.user.id)}.png'))
            await interaction.response.send_message(f"Ich habe dir eine Nachricht mit deiner Spielzeit gesendet! {msg.jump_url}", ephemeral=True)
        else:
            text = "Bisher hast du noch nichts gespielt."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /gametime_all ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


@bot.command(
    name="gametime_get",
    description="Liefert die Zeit eines bestimmten Spieles in Minuten", )
async def gametime_get(interaction: discord.Interaction, name: discord.Option(str, required=True, description="Name des Spieles"),
                       tage: discord.Option(int, required=False, description="Zeitraum in Tage")):
    """
    This function returns the time played for a specific game in minutes or hours, depending on the game time duration.
    Args:
        :param tage: An integer representing the number of days to get the time played for. If not specified, all time played for the game will be returned.
        :param name: A string representing the name of the game to get the time played for.
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        if tage is None:
            tmp = gametime.get_games(int(interaction.user.id))
            for i in tmp:
                score = SequenceMatcher(None, i[0], name).ratio()
                if score >= 0.80:
                    name = i[0]
                    game_time = float(i[1])
                    if game_time >= 3600:
                        game_time = round(game_time / 3600, 2)
                        text = f"**{name}** wurde von dir `{game_time}` Stunden gespielt."
                    else:
                        game_time = round(game_time / 60, 2)
                        text = f"**{name}** wurde von dir `{game_time}` Minuten gespielt."
                    log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
                    await interaction.response.send_message(text)
            text = "Ich konnte das Spiel nicht finden."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text, ephemeral=True)
        else:
            tmp = gametime.get_games_days(int(interaction.user.id), tage)
            total_time = 0
            for i in tmp:
                score = SequenceMatcher(None, i[0], name).ratio()
                if score >= 0.80:
                    name = i[0]
                    game_time = float(i[1])
                    total_time += game_time
            if total_time == 0:
                text = f"Ich konnte **{name}** nicht mit genaueren Daten finden. Bitte versuche es ohne `tage` noch mal."
                log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
                await interaction.response.send_message(text, ephemeral=True)
            elif total_time >= 3600:
                total_time = round(total_time / 3600, 2)
                text = f"**{name}** wurde von dir in den letzten `{tage}` Tagen `{total_time}` Stunden gespielt."
            else:
                total_time = round(total_time / 60, 2)
                text = f"**{name}** wurde von dir in den letzten `{tage}` Tagen `{total_time}` Minuten gespielt."
            log.add(log_file, f"{interaction.user} ({interaction.user.id}) - {text}")
            await interaction.response.send_message(text)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /gametime_get ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


# Foto ____________________________________________________


# Sonstiges _______________________________________________

@bot.command(
    name="helpme",
    description="Wie funktioniert das hier?", )
async def helpme(interaction: discord.Interaction):
    """
    A command function that provides help text to the user.
    Args:
        :param interaction: Represents a Discord interaction between User and Server
    """
    try:
        text = helptext.text
        log.add(log_file, f"{interaction.user} ({interaction.user.id}) - hat hilfe ausgeführt.")
        await interaction.response.send_message(text, ephemeral=True)
    except Exception as e:
        text = f"{interaction.user} ({interaction.user.id}) hat beim Ausführen vom /helpme ein '{e}' bekommen."
        log.add(log_file, text)
        await interaction.response.send_message(f"Ein Fehler ist aufgetreten: {e}", ephemeral=True)


# At user change _____________________________________

@bot.event
async def on_presence_update(before, after):
    """
    Event handler that triggers when a user's presence is updated.
    This function updates the user's gaming time based on their presence changes.
    Args:
        before (discord.Presence): The user's presence before the update.
        after (discord.Presence): The user's presence after the update.
    """
    user = after.id
    role = discord.utils.get(after.roles, id=gametime_role)
    if str(role) == "spielzeit":
        game_before = [i for i in before.activities if str(i.type) == "ActivityType.playing"]
        if game_before:
            before = game_before[0].name
        else:
            before = None
        game_after = [i for i in after.activities if str(i.type) == "ActivityType.playing"]
        if game_after:
            after = game_after[0].name
        else:
            after = None

        for i in gametime.get_blacklist(user):
            if i[0] == before:
                before = None
            if i[0] == after:
                after = None

        if before == after:
            pass
        elif before is None:
            if after is None:
                text = None
            else:
                gametime.set_start_time(user, after)
                text = f"{user} - hat nichts gespielt und spielt jetzt {after}."
                log.add(log_file, text)
        elif after is None:
            text = f"{user} - hat {before} gespielt und spielt jetzt nichts."
            gametime.set_total_time(user, before)
            log.add(log_file, text)
        else:
            text = f"{user} - hat {before} gespielt und spielt jetzt {after}."
            gametime.set_total_time(user, before)
            gametime.set_start_time(user, after)
            log.add(log_file, text)


@bot.event
async def on_voice_state_update(member, before, after):
    """
    An event listener function that listens for voice state updates in a Discord server.
    Creates a new voice channel named "Voicechannel" and moves the user to that channel when the user joins
    a specific voice channel defined in `new_channel`. Sets specific permissions for the user in the newly created voice channel.
    If the user leaves the voice channel, checks if the channel is empty and deletes it if so.
    Args:
        member: A Discord Member object representing the member whose voice state was updated
        before: A Discord VoiceState object representing the user's voice state before the update
        after: A Discord VoiceState object representing the user's voice state after the update
    """
    new_channel_all = [1026175404307906641, 1150864930388586557]
    new_channel_irl = [1150862000549802086, 1150864499818123404]
    try:
        if after.channel.id in new_channel_all:
            name = "Voicechannel"
            await after.channel.category.create_voice_channel(name, bitrate=int(member.guild.bitrate_limit))
            for chv in after.channel.category.voice_channels:
                if chv.name == name:
                    await member.move_to(chv)
                    ov = chv.overwrites_for(member) or discord.PermissionOverwrite()
                    ov.manage_channels = True
                    await chv.set_permissions(member, overwrite=ov)

        elif after.channel.id in new_channel_irl:
            if secret.get_debug() is True:
                role_id = 1056180541994573824
            else:
                role_id = 939961746624483438
            name = "Voicechannel"
            role = member.guild.get_role(role_id)
            if role is not None:
                category = after.channel.category
                await category.create_voice_channel(name, bitrate=int(member.guild.bitrate_limit))

                for chv in category.voice_channels:
                    if chv.name == name:
                        await member.move_to(chv)

                        # @everyone-Berechtigungen überschreiben
                        ov_everyone = chv.overwrites_for(member.guild.default_role) or discord.PermissionOverwrite()
                        ov_everyone.view_channel = False  # @everyone darf den Kanal nicht sehen
                        await chv.set_permissions(member.guild.default_role, overwrite=ov_everyone)

                        # Rollenberechtigungen festlegen
                        ov_role = chv.overwrites_for(role) or discord.PermissionOverwrite()
                        ov_role.view_channel = True  # Die gewünschte Rolle darf den Kanal sehen
                        await chv.set_permissions(role, overwrite=ov_role)
    except AttributeError:
        pass
    try:
        channels = new_channel_all + new_channel_irl
        if after.channel is None or after.channel.id is not before.channel.id:
            for chv in before.channel.category.voice_channels:
                if len(chv.voice_states) == 0 and chv.id not in channels:
                    await chv.delete()
    except AttributeError:
        pass


# __________________________________________________________________________________________________________

bot.run(secret.apptoken)
