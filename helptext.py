text = """
> **Wie funktionieren die Bots hier?**

**CakeBot:**
Sobald du dir die Rolle ***spielzeit*** in <#939963107797778472> abgeholt hast, loggt der Bot automatisch, vorausgesetzt es wird in Discord angezeigt, deine gestarteten und beendeten Spiele. Mit folgenden Befehlen kannst du diese manipulieren oder komplett löschen:

:small_blue_diamond: `/gametime_all` Gesamtspielzeit als Liste und Kuchen senden lassen
:small_blue_diamond: `/gametime_get` Liefert die Zeit eines bestimmten Spieles
:small_blue_diamond: `/gametime_remove <name>` Einen speziellen Spiel-Eintrag löschen
:small_blue_diamond: `/gametime_clear <Mit 'Y' bestätigen>` Alle deine Spiele-Einträge löschen
:small_blue_diamond: `/blacklist_add  <name>` Spiel zur Blacklist hinzufügen
:small_blue_diamond: `/blacklist_remove  <name>` Spiel von der Blacklist entfernen
:small_blue_diamond: `/blacklist_clear <Mit 'Y' bestätigen>` Alle deine Blacklist-Einträge löschen
:small_blue_diamond: `/blacklist_get` Zeigt alle Blacklist Einträge

**MusikBot:**
Verarbeitet aktuell YouTube Links und macht diese hübsch im Channel <#942494220894236693>:

:small_blue_diamond: `/music <YouTube-Link>` Um einen YouTube Link im Channel zu senden

**Sonstiges:**

:small_blue_diamond: `/helpme` Dieser Befehl
"""
