import time
import secret
import mysql.connector

# Create shared database connection and cursor
if secret.get_debug():
    db_conn = mysql.connector.connect(**secret.config_gametime_debug)
else:
    db_conn = mysql.connector.connect(**secret.config_gametime_prod)
    db_conn.set_charset_collation('utf8mb4')
db_cursor = db_conn.cursor(prepared=True)


def sql_set(query, params=None):
    try:
        db_cursor.execute(query, params)
        db_conn.commit()
    except mysql.connector.OperationalError as e:
        if e.errno == 2013 or e.errno == 2006:
            # Reconnect if connection is lost
            db_conn.reconnect(attempts=3, delay=1)
            db_cursor.execute(query, params)
            db_conn.commit()
        else:
            raise e


def sql_get(query, params=None):
    try:
        db_cursor.execute(query, params)
        return db_cursor.fetchall()
    except mysql.connector.OperationalError as e:
        if e.errno == 2013 or e.errno == 2006:
            # Reconnect if connection is lost
            db_conn.reconnect(attempts=3, delay=1)
            db_cursor.execute(query, params)
            db_conn.commit()
        else:
            raise e


def copy_db():
    db_cursor.execute("TRUNCATE gametime_debug.gametime")
    db_cursor.execute("TRUNCATE gametime_debug.blacklist")
    db_cursor.execute("TRUNCATE gametime_debug.session")
    db_cursor.execute("INSERT INTO gametime_debug.gametime SELECT * FROM gametime.gametime")
    db_cursor.execute("INSERT INTO gametime_debug.blacklist SELECT * from gametime.blacklist")
    db_cursor.execute("INSERT INTO gametime_debug.session SELECT * from gametime.session")
    db_conn.commit()


# Cleanup _______________________________________


def clear_start_time():
    """
    Deletes the start_time from every user and updates it with the unix timestamp at the
    time of execution.
    """
    sql_set("UPDATE gametime SET start_time = %s", (time.time(),))


def clear_gametime_entry(discord_id, game_name):
    """
    Deletes an entry of a user
    :param discord_id: Discord-ID of the user who wants to delete the entry
    :param game_name: Name of the game to delete
    """
    result = sql_get("SELECT * FROM gametime WHERE discord_id = %s AND game_name = %s", (discord_id, game_name,))
    if result is None:
        return 2
    else:
        sql_set("DELETE FROM gametime WHERE discord_id = %s AND game_name = %s", (discord_id, game_name,))
        return 1


def clear_gametime_all(discord_id):
    """
    Deletes everything entry of a user
    :param discord_id: Discord-ID of the user who wants to delete the entry
    """
    result = sql_get("SELECT * FROM gametime WHERE discord_id = %s", (discord_id,))
    if len(result) == 0:
        return 2
    else:
        sql_set("DELETE FROM gametime WHERE discord_id = %s", (discord_id,))
        return 1


def clear_blacklist_entry(discord_id, game_name):
    """
    Deletes the given entry of a users blacklist
    :param discord_id: Discord-ID of the user who wants to delete the entry
    :param game_name: Name of the game to delete

    :return: 2 if the entry was already in the blacklist, 1 if the entry was successful, 0 if an exception occurred
    """

    result = sql_get("SELECT game_name FROM blacklist WHERE discord_id = %s AND game_name = %s", (discord_id, game_name,))
    if result is None:
        return 2
    else:
        sql_set("DELETE FROM blacklist WHERE discord_id = %s AND game_name = %s", (discord_id, game_name))
        return 1


def clear_blacklist_all(discord_id):
    """
    Deletes everything entry of a users blacklist
    :param discord_id: Discord-ID of the user who wants to delete the entry
    """
    result = sql_get("SELECT * FROM blacklist WHERE discord_id = %s", (discord_id,))
    if result is None:
        return 2
    else:
        sql_set("DELETE FROM blacklist WHERE discord_id = %s", (discord_id,))
        return 1


# Getter ________________________________________

def get_total_time(discord_id, game_name):
    """
    Returns the total time played by certain game and user
    :param discord_id: Discord-ID of the user of which data to get
    :param game_name: Name of the game of which time to get

    :return: the matched entry of the database as a tuple, if no entry was found, you'll get a None
    """
    result = sql_get("SELECT total_time FROM gametime WHERE discord_id = %s AND game_name = %s", (discord_id, game_name))
    if result[0] is None:
        return float(0)
    else:
        return result[0]


def get_start_time(discord_id, game_name):
    """
    Returns the time when a user started a certain game
    :param discord_id: Discord-ID of the user of which data to get
    :param game_name: Name of the game of which time to get

    :return: the matched entry of the database as a tuple, if no entry was found, you'll get a None
    """
    return sql_get("SELECT start_time FROM gametime WHERE discord_id = %s AND game_name = %s", (discord_id, game_name))[0]


def get_games(discord_id):
    """
    Returns all played games and the time by user
    :param discord_id: Discord-ID of the user of which data to get

    :return: the matched entries of the database as a list, if no entry was found, you'll get an empty list
    """
    return sql_get("SELECT game_name, total_time FROM gametime WHERE discord_id = %s ORDER BY CAST(total_time as int) DESC", (discord_id,))


def get_blacklist(discord_id):
    """
    Returns all blacklisted games by user
    :param discord_id: Discord-ID of the user of which data to get

    :return: the matched entries of the database as a list, if no entry was found, you'll get an empty list
    """
    return sql_get("SELECT game_name FROM blacklist WHERE discord_id = %s", (discord_id,))


def get_games_days(discord_id, days):
    """
    Returns the gametime of given days
    :param discord_id: Discord-ID of the user of which data to get
    :param days: Days of which data to get
    :return: The matched entries of the database as a list, if no entry was found, you'll get an empty list
    """
    if days is None:
        return sql_get("SELECT game_name, total_time FROM gametime WHERE discord_id = %s ORDER BY CAST(total_time as int) DESC", (discord_id,))
    else:
        days_in_unix = days * 86400
        unix_period = time.time() - days_in_unix
        return sql_get("SELECT game_name, time_in_session FROM session WHERE discord_id = %s AND unix_time_stamp > %s ORDER BY CAST(unix_time_stamp as int) DESC", (discord_id, unix_period))


# Setter ________________________________________

def set_total_time(discord_id, game_name):
    """
    Adds/Updates the total time a game was played after it was closed
    total_time will be calculated with the current unix time stamp minus the start_time of a game plus the total_time at this point
    :param discord_id: Discord-ID of the user of which data to get
    :param game_name: Name of the game of which time to get
    """
    start_time = get_start_time(discord_id, game_name)[0]
    total_time = time.time() - float(start_time)
    current_total = get_total_time(discord_id, game_name)[0]
    if current_total is None:
        current_total = 0
    total_time = float(current_total) + total_time
    sql_set("UPDATE gametime SET total_time = %s WHERE discord_id = %s AND game_name = %s", (total_time, discord_id, game_name,))
    sql_set("INSERT INTO session (discord_id, game_name, unix_time_stamp, time_in_session) VALUES (%s, %s, %s, %s)", (discord_id, game_name, time.time(), time.time() - float(start_time),))


def set_start_time(discord_id, game_name):
    """
    Adds the start time when a user started a certain game
    :param discord_id: Discord-ID of the user of which data to get
    :param game_name: Name of the game of which time to get
    """
    result = sql_get("SELECT * FROM gametime WHERE discord_id = %s AND game_name = %s", (discord_id, game_name,))
    if len(result) == 0:
        sql_set("INSERT INTO gametime (discord_id, game_name, start_time, total_time) VALUES (%s, %s, %s, 0)", (discord_id, game_name, time.time(),))
        return 1
    else:
        sql_set("UPDATE gametime SET start_time = %s WHERE discord_id = %s AND game_name = %s", (time.time(), discord_id, game_name,))
        return 1


def set_blacklist(discord_id, game_name):
    """
    Adds a game to a users blacklist
    :param discord_id: Discord-ID of the user's blacklist
    :param game_name: Name of the game of which to set on the blacklist

    :return: true if the entry was successful, false if an exception occurred
    """
    result = sql_get("SELECT * FROM blacklist WHERE discord_id = %s AND game_name = %s", (discord_id, game_name,))
    if len(result) == 0:
        sql_set("INSERT INTO blacklist (discord_id, game_name) VALUES (%s, %s)", (discord_id, game_name))
        return 1
    else:
        return 2
