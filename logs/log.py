from datetime import datetime


def add(log_file, text):
    text = text.replace("\n", " - ")
    if log_file == "main_debug":
        file = "logs/debug/main_debug.log"
    elif log_file == "main":
        file = "logs/live/main.log"
    else:
        file = "../logs/tmp.log"
    f = open(file, "a")
    tmp = str(datetime.now()) + " " + text
    print(tmp)
    tmp += "\n"
    f.write(tmp)
    f.close()
