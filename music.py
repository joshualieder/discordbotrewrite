"main file for the music finder"
import os
import googleapiclient.discovery
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import secret
from odesli.Odesli import Odesli


def get_song_and_artist_information_from_spotify(song_url: str):
    """
    Function to get information about a song using its Spotify URL.
    """

    spotify_api = spotipy.Spotify(
        auth_manager=SpotifyClientCredentials(
            client_id=secret.SPOTIFY_CLIENT_ID,
            client_secret=secret.SPOTIFY_SECRET))

    result_song = None
    result_artist = None

    # Get information from Spotify URL
    try:
        track_id = song_url.split('/')[-1]  # Extract track ID from URL
        result_song = spotify_api.track(track_id)
        result_artist = spotify_api.artist(result_song['artists'][0]['uri'])
    except (IndexError, KeyError, TypeError):
        pass

    return result_song, result_artist


def suche(youtube_id: str = None, artist: str = None, title: str = None):
    youtube_link = f"https://youtu.be/{youtube_id}"

    odesli = Odesli()
    result = odesli.getByUrl(youtube_link)

    if youtube_link is not None:
        result = odesli.getByUrl(youtube_link)
        spotify_url = result.songsByProvider['spotify'].linksByPlatform['spotify']

    # since artist and title are now filled the spotify search can start
    results_spotify = get_song_and_artist_information_from_spotify(spotify_url)
    result_song = results_spotify[0]
    result_artist = results_spotify[1]

    # create some clean json from the spotify and YouTube output
    clean_result: dict = {
        "artist": result_artist['name'],
        "title": result_song['name'],
        "release": result_song['album']['release_date'],
        "cover": result_song['album']['images'][1],
        "artist_icon": result_artist['images'][1],
        "genres": result_artist['genres'],
        "spotify_link": result_song['external_urls']['spotify'],
        "youtube_link": youtube_link
    }

    return clean_result
